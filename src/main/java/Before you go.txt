#Before You Go

practice list up to 3 months

Java input and output from console // done
Simple arithmetic and bitwise operation // done
IF-Else // done
Loop // done
Array // done
ArrayList // data structure
List // data structure
HashMap // data structure
Stack class // data structure
Queue class // data structure
StringBuffer class // data structure
String class
Date and time related classes
File input and output
Linear Search
Binary Search
Bubble Sort
Quick Sort
Sieve of Eratosthenes
DFS
BFS
Inheritance
Interface
Abstraction
Polymorphism
Method overloading
Method overriding