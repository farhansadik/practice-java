package Programs.HomeManagement;

import java.sql.*;

public class Query {
    static final String url = "jdbc:mysql://localhost:3306/";
    static final String username = "root";
    static final String password = "12345";

    // connect to database
    public Connection databaseConnection() throws SQLException {

        // Registering the Driver
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch(Exception exception){ System.out.println(exception); }

        // Getting the connection
        Connection mySqlCon = DriverManager.getConnection(url, username, password);
        System.out.println("[*] Connected to MySQL database");
        return mySqlCon;
    }

    public void closeConnection() throws SQLException {
        System.out.println("[*] close connection ");
        Connection c = DriverManager.getConnection(url, username, password);
        c.close();
        System.out.println("[*] exit ");
        System.exit(0);
    }

    // create database
    public void preDatabase(Connection con, Statement statement) throws SQLException {

        // create database
        String db_name = "home_management";
        statement.executeUpdate("CREATE DATABASE IF NOT EXISTS " + db_name);
        System.out.println("[*] Database Created ");

        // select database
        String select_database = "jdbc:mysql://localhost:3306/home_management";
        try(Connection conn = DriverManager.getConnection(select_database, username, password);) {
            Statement stmt = conn.createStatement();
            System.out.println("[*] Selected Database : " + db_name);

            // creating table
            String table_name = "tablea";
            String table_1 = "CREATE TABLE IF NOT EXISTS " + table_name +
                    "(id INT NOT NULL AUTO_INCREMENT, " +
                    "name VARCHAR(25), " +
                    "roll int, " +
                    "PRIMARY KEY (id));";
            stmt.executeUpdate(table_1);
            System.out.println("[*] Table has been created");
            System.out.println("[*] Selected Table " + table_name);

        } catch (SQLException e) { e.printStackTrace(); }

    }

    // add records
    public void addRecords(Connection connection, String name, int roll) {

        // select database
        String select_database = "jdbc:mysql://localhost:3306/home_management";
        try(Connection conn = DriverManager.getConnection(select_database, username, password);) {
            Statement stmt = conn.createStatement();

            //  query
            String table_name = "tablea";
            String sql_1 = "INSERT INTO " + table_name + "(name, roll)" +
                    "VALUES('" + name +"'," + roll +");" ;
            stmt.executeUpdate(sql_1);
            System.out.println("[*] added name : " + name);
            System.out.println("[*] added roll : " + roll);

        } catch (SQLException e) { e.printStackTrace(); }
    }

    // view records
    public void viewRecords(Connection con) throws SQLException {
        String select_database = "jdbc:mysql://localhost:3306/home_management";
        try(Connection conn = DriverManager.getConnection(select_database, username, password);) {
            Statement stmt = conn.createStatement();

            String table_name = "tablea";

            // printing table
            ResultSet resultSet = stmt.executeQuery("SELECT * FROM " + table_name);
            System.out.println("\nTable Info");
            while (resultSet.next()) {
                System.out.print(resultSet.getInt(1) + "\t");
                System.out.print(resultSet.getString(2) + " (");
                System.out.print(resultSet.getInt(3) + ")\n");
            }

        } catch (SQLException e) { e.printStackTrace(); }
    }

    // update records
    public void updateRecords(Connection con, String name , int roll, int id) throws SQLException {
        String select_database = "jdbc:mysql://localhost:3306/home_management";
        try(Connection conn = DriverManager.getConnection(select_database, username, password);) {
            Statement stmt = conn.createStatement();

            String table_name = "tablea";

            // update tablea set name = 'China', roll = 109 where id = 6;
            String sql = "UPDATE " + table_name +
                    " SET name = '" + name + "'," +
                    " roll = " + roll +
                    " WHERE id = " + id;
            stmt.executeUpdate(sql);
            System.out.println("[*] id '" + id + "' has been updated");

        } catch (SQLException e) { e.printStackTrace(); }
    }

    // remove records
    public void dropRecords(Connection con, int id) throws SQLException {
        String select_database = "jdbc:mysql://localhost:3306/home_management";
        try(Connection conn = DriverManager.getConnection(select_database, username, password);) {
            Statement stmt = conn.createStatement();

            String table_name = "tablea";

            String sql = "DELETE FROM " + table_name + " WHERE id = " + id;
            stmt.executeUpdate(sql);
            System.out.println("[*] id '" + id + "' has been removed");

        } catch (SQLException e) { e.printStackTrace(); }
    }

    // drop database
    public void dropDatabase(Connection con, Statement statement, String name) throws SQLException {
        // executing query
        String query = "DROP DATABASE " + name;
        statement.executeUpdate(query);
        System.out.println(" '" +name + "' database has been deleted!");
    }
}
