package Programs.HomeManagement;

import java.sql.Connection;
import java.sql.Statement;
import java.util.Scanner;

public class Main {
    static final String version="v0.29 Alpha";

    public static void main(String[] args) throws Exception {

        Query query = new Query();
        Scanner input = new Scanner(System.in);

        System.out.println(version);

        Connection connection = query.databaseConnection();
        Statement statement = connection.createStatement();
        System.out.println("[*] loading pre database");
        query.preDatabase(connection, statement);

        System.out.println();
        mainMenu();
        System.out.print("\nconsole % ");
        int getInput = input.nextInt();

        if ( getInput == 1 ) {
            // view records
            query.viewRecords(connection);
        } else if (getInput == 2) {
            // printing previous table
            query.viewRecords(connection);

            // add records
            System.out.println("\n[*] enter 0 for exit");
            System.out.print("enter name > ");
            input.nextLine();
            String name = input.nextLine();

            if (name .equals("0")) {
                System.out.println("exit");
                System.exit(0);
            } else {
                System.out.print("enter roll > ");
                int roll = input.nextInt();

                query.addRecords(connection, name, roll);

                // printing table after adding data
                query.viewRecords(connection);
            }

        } else if (getInput == 3) {
            // update records
            // printing previous table
            query.viewRecords(connection);

            // add records
            System.out.println("\n[*] enter 0 for exit");

            System.out.print("enter id > ");
            int id = input.nextInt();

            if ( id == 0 ) {
                System.out.println("exit");
                System.exit(0);
            } else {
                System.out.print("enter name > ");
                input.nextLine();
                String name = input.nextLine();

                System.out.print("enter roll > ");
                int roll = input.nextInt();

                query.updateRecords(connection, name, roll, id);

                // printing table after adding data
                query.viewRecords(connection);
            }
        } else if (getInput == 4) {
            // remove records
            // printing previous table
            query.viewRecords(connection);

            // remove
            System.out.println("\n[*] enter 0 for exit");
            System.out.print("enter id > ");
            int id = input.nextInt();

            if (id == 0 ) {
                System.out.println("exit");
                System.exit(0);
            } else {
                query.dropRecords(connection, id);

                // printing table after adding data
                query.viewRecords(connection);
            }
        } else {
            System.out.println("this is null");
        }

        // close connection
        System.out.println();
        query.closeConnection();
    }

    public static void mainMenu() {
        System.out.println("1. View Records");
        System.out.println("2. Add Records");
        System.out.println("3. Update Records");
        System.out.println("4. Remove Records");
    }
}
