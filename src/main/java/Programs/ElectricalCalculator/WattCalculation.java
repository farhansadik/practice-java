package Programs.ElectricalCalculator;

import java.util.Scanner;

public class WattCalculation {

    // TESTED AND OK

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int current, voltage;
        double pf;

        System.out.print("current : ");
        current = input.nextInt();

        System.out.print("voltage : ");
        voltage = input.nextInt();

        System.out.print("pf : ");
        pf = input.nextDouble();

        if (pf <= 1) {

            // converting integer to double
            double doubleSum = (double) voltage * (double) current * pf;

            // converting double to integer
            int intSum = (int) doubleSum;

            if (doubleSum <= 1000) {
                System.out.println("power/watt = " + intSum + " watt");
            } else if (doubleSum >= 1000000) {
                System.out.println("power/watt = " + doubleSum / 1000000 + " mega watt");
            } else if (doubleSum > 1000) {
                double r = doubleSum / 1000;
                System.out.println("power/watt = " + doubleSum / 1000 + " kilo watt");
            } else {
                System.out.println("power/watt = " + doubleSum + " error with watt");
            }

        } else {
            System.out.println("error in pf > 1");
        }

    }
}
