package Programs.Stopwatch;

public class Stopwatch {

    public static void main(String[] args) {

        System.out.println("Seconds : ");
        calculateSeconds();
        clearScreen();
        calculateSeconds();

    }

    public static void clearScreen() {

        /* method 1
        System.out.print("\033[H\033[2J");
        System.out.flush();
        */

        for(int i = 0; i < 80*300; i++) { // Default Height of cmd is 300 and Default width is 80
            System.out.print("\b"); // Prints a backspace
        }

    }

    static int calculateSeconds(){

        int startValue = 58;
        int endValue = 65;

        while (startValue <= endValue) {
            delay(); // for delay
            System.out.print("\b\b" + startValue);

            // limit point
            if (startValue == 60) {

                //System.out.println("\nBreak");
                int i=0;
                System.out.println("\nHour : \n" + i+1);

                break;
            }
            startValue++;
        }
        return startValue;
    }

    // delay method
    public static void delay() {
        int delay = 500;
        try {
            Thread.sleep(delay); // In your case it would be: Thread.sleep(100);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

}
