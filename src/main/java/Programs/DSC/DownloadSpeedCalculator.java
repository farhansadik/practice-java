package Programs.DSC;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Scanner;

public class DownloadSpeedCalculator {

    public static double download_speed;
    public static double file_size;

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        NumberFormat number = new DecimalFormat("#0.00");

        System.out.println("This program will execute only in GB");
        System.out.print("Enter Total File Size : ");
        file_size = input.nextDouble();
        System.out.print("Enter Download Speed  : ");
        download_speed = input.nextDouble();

        System.out.println("Required Time : " + number.format(megaPerSec()) + " h");
    }

    public static double megaPerSec() {

        // 1gb = 1024 mb
        double mb = 1024 * file_size; // size in megabyte

        // downloading per sec
        double sec = 1 * mb / download_speed; // downloading per sec

        // 60 sec = 1 min
        double min = 1 * sec / 60; // downloading per min

        // 60 min = 1 h
        double result = 1 * min / 60; // downloading per hour

        return result;
    }
}
