package Database;
import java.sql.*;

public class InsertRecord {
    public static void main(String[] args) throws Exception {

        // load and register driver
        Class.forName("com.mysql.cj.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/custom_database"; // selected custom database
        Connection connection= DriverManager.getConnection(url, "root", "12345");
        Statement statement=connection.createStatement();

        // executing query
        String table_1 = "teachers";
        String sql_1 = "INSERT INTO " + table_1 +
                "(name, post) VALUES('Robi Mia', 'Math');" ;
        statement.executeUpdate(sql_1);
        System.out.println("For teachers data has been added");

        String table_2 = "students";
        String sql_2 = "INSERT INTO " + table_2 +
                "(name, roll) VALUES('Jakaria Sheikh', 100);" ;
        statement.executeUpdate(sql_2);
        System.out.println("For students data has been added");

        // close connection
        connection.close();
    }
}
