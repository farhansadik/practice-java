package Database;
import java.sql.*;

class FirstExample{

    // getConnection parameters
    static final String url = "jdbc:mysql://localhost:3306/mydb";
    static final String username = "root";
    static final String password = "12345";

    public static void main(String args[]){
        try {
            // load and register driver
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con=DriverManager.getConnection(url, username, password);

            // connection statement
            Statement st=con.createStatement();

            // executing query
            ResultSet rs=st.executeQuery("SELECT * FROM students");
            
            // getting result
            while(rs.next()) {
                System.out.println(
                        // id
                        rs.getInt(1)
                        // name
                        + "  " + rs.getString(2)
                        // roll
                        + "  " + rs.getString(3));
            }

            // close connection
            con.close();

        } catch(Exception e){ System.out.println(e); }
    }
}