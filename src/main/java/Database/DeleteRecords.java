package Database;
import java.sql.*;

public class DeleteRecords {
    public static void main(String[] args) throws Exception {

        // load and register driver
        Class.forName("com.mysql.cj.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/custom_database"; // selected custom database
        Connection connection= DriverManager.getConnection(url, "root", "12345");
        Statement statement=connection.createStatement();

        // executing query
        String table = "students";
        int id = 6;

        // delete from students where id = 7;
        String sql = "DELETE FROM " + table + " WHERE id = " + id;
        statement.executeUpdate(sql);

        // close connection
        connection.close();
    }
}
