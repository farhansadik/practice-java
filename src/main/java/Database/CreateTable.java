package Database;
import java.sql.*;

public class CreateTable {
    public static void main(String[] args) throws Exception {

        /*
            USING DATABASE ~ custom_database
         */

        // load and register driver
        Class.forName("com.mysql.cj.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/custom_database"; // selected database
        Connection connection= DriverManager.getConnection(url, "root", "12345");

        // connection statement
        Statement statement=connection.createStatement();

        // executing query
        String table_1 = "CREATE TABLE teachers" +
                "(id INT NOT NULL AUTO_INCREMENT, " +
                "name VARCHAR(255), " +
                "post VARCHAR(255), " +
                "PRIMARY KEY (id));";
        String table_2 = "CREATE TABLE students" +
                "(id INT NOT NULL AUTO_INCREMENT, " +
                "name VARCHAR(255), " +
                "roll int, " +
                "PRIMARY KEY (id));";

        statement.executeUpdate(table_1);
        statement.executeUpdate(table_2);
        System.out.println("table has been deleted....!");

        // close connection
        connection.close();

    }
}
