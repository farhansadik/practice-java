package Database;
import java.sql.*;

public class ViewRecords {
    public static void main(String[] args) throws Exception {

        // load and register driver
        Class.forName("com.mysql.cj.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/custom_database"; // selected custom database
        Connection connection= DriverManager.getConnection(url, "root", "12345");
        Statement statement=connection.createStatement();

        // executing query
        String table = "teachers";
        String sql = "SELECT * FROM " + table;

        // printing result
        ResultSet resultSet = statement.executeQuery(sql);
        System.out.printf("%-2s %-16s %-10s\n", "id", "name", "post");
        while(resultSet.next()) {
            System.out.println(
                    // id
                    resultSet.getInt(1) // 1 - column index
                    // name
                    + "  " + resultSet.getString(2)
                    // post
                    + "  " + resultSet.getString(3));
        }

        System.out.println("\n");
        String table2 = "students";
        String sql2 = "SELECT * FROM " + table2;
        ResultSet rs = statement.executeQuery(sql2);

        while (rs.next()) {
            System.out.println("ID : " + rs.getInt("id"));
            System.out.println("Name : " + rs.getString(2));
            System.out.println("Roll : " + rs.getInt("roll"));
        }

        // close connection
        connection.close();
    }
}
