package Database;
import java.sql.*;

public class UpdateRecords {
    public static void main(String[] args) throws Exception {

        // load and register driver
        Class.forName("com.mysql.cj.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/custom_database"; // selected custom database
        Connection connection= DriverManager.getConnection(url, "root", "12345");
        Statement statement=connection.createStatement();

        // executing query
        String table = "students";
        String update_name = "Umayer";
        int update_id = 4;

        String sql = "UPDATE " + table + " SET name = '" + update_name + "'" +
                " WHERE id = " + update_id;
        statement.executeUpdate(sql);

        // close connection
        connection.close();
    }
}
