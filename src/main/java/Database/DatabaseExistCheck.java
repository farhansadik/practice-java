package Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DatabaseExistCheck {
    public static void main(String[] args) throws Exception {

        // load and register driver
        Class.forName("com.mysql.cj.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/";
        Connection connection= DriverManager.getConnection(url, "root", "12345");

        // connection statement
        Statement statement=connection.createStatement();

        // executing query
        // Connection connection = <your java.sql.Connection>
        ResultSet resultSet = connection.getMetaData().getCatalogs();

        while (resultSet.next()) {
            // Get the database name, which is at position 1
            String databaseName = resultSet.getString(1);
            if (databaseName.equals("mydb11")) {
                System.out.print("yes");
                //Statement stmt = connection.createStatement();
                //String sql = "DROP DATABASE library";
                //stmt.executeUpdate(sql);
            }
        /*
        //iterate each catalog in the ResultSet
        while (resultSet.next()) {
            // Get the database name, which is at position 1
            String custom_database1 = resultSet.getString(1);
        }
        resultSet.close();
        */
            // close connection
            connection.close();
        }

    }
}
