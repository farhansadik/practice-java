package Database;

import java.sql.*;

public class CreateDatabase {
    public static void main(String[] args) throws Exception {

        // load and register driver
        Class.forName("com.mysql.cj.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/mydb";
        Connection connection= DriverManager.getConnection(url, "root", "12345");

        // connection statement
        Statement statement=connection.createStatement();

        // executing query
        String input = "custom_database";
        statement.executeUpdate("CREATE DATABASE " + input);

        // close connection
        connection.close();

    }
}
