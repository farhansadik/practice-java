package Database;

import java.sql.*;

public class Multiple_DBs {

    /*
        A TEST FOR SEPARATE METHOD
     */

    // connect to database
    public Connection connectToMySQL() throws SQLException{

        // Registering the Driver
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch(Exception exception){ System.out.println(exception); }

        // Getting the connection
        String url = "jdbc:mysql://localhost:3306/mydb";
        Connection mySqlCon = DriverManager.getConnection(url, "root", "12345");
        System.out.println("Connected to MySQL database......");
        return mySqlCon;
    }

    // executing and printing query
    public void ExtractDataFromMySQL(Connection con) throws SQLException {

        //Creating the Statement
        Statement statement = con.createStatement();

        //Executing the query
        String table_name = "students";
        ResultSet resultSet = statement.executeQuery("SELECT * FROM " + table_name + ";");
        System.out.println("Contents of 'students' table in MySQL database: ");

        while(resultSet.next()) {
            System.out.print("ID: " + resultSet.getInt("id")+", ");
            System.out.print("Name: " + resultSet.getString("names")+", ");
            System.out.print("Age: " + resultSet.getString("age"));
            System.out.println();
        }
        System.out.println();
    }

    public static void main(String[] args) throws Exception {

        Multiple_DBs object = new Multiple_DBs();

        //Connecting to MySQL
        Connection msqlCon = object.connectToMySQL();

        //Extracting data from MySQL
        object.ExtractDataFromMySQL(msqlCon);
    }
}
