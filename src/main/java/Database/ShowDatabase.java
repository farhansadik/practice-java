package Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class ShowDatabase {
    public static void main(String[] args) throws Exception {

        // load and register driver
        Class.forName("com.mysql.cj.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/custom_database"; // selected custom database
        Connection connection= DriverManager.getConnection(url, "root", "12345");
        Statement statement=connection.createStatement();

        // executing query
        String query = "SHOW DATABASES;";
        ResultSet resultSet = statement.executeQuery(query);

        while (resultSet.next()) {
            System.out.println(resultSet.getString(1));
            //System.out.println();

        }


        // close connection
        connection.close();
    }
}
