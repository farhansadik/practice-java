package Problem_Solving;

import java.util.Scanner;

public class Age_in_days_1020 {
    public static void main(String[] args) {

        // problem 1020 - Age in Days
        // uri online
        // solved

        Scanner getUserInput = new Scanner (System.in);

        int input = getUserInput.nextInt();

        int year = input / 365;
        System.out.println(year + " ano(s)");

        if (input > 365) {
            int temp = year * 365; // actual day
            int temp1 = input - temp;  // left day
            int month = temp1 / 30;
            System.out.println(month + " mes(es)");

            int temp2 = month * 30; // actual month
            int day = temp1 - temp2;
            System.out.println(day + " dia(s)");

        } else if ( input <= 365) {
            int month = input / 30;
            System.out.println(month + " mes(es)");

            int temp = month * 30; // actual month
            int day = input - temp;
            System.out.println(day + " dia(s)");
        }

    }
}
