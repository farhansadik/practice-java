package Problem_Solving;

import java.io.File;
import java.io.FileWriter;

public class CreateAFile {

    // OK

    public static void main(String[] args) throws Exception {
        // to create file
        String desktop = "C:\\Users\\Farhan Sadik\\Desktop\\";
        File my_file = new File(desktop + "MyTextFile.txt");

        // if condition - for check existence of file
        if (!my_file.exists()) my_file.createNewFile();

        // writing file
        FileWriter file_writer = new FileWriter(my_file, true);
        System.out.println("Creating File.....");
        file_writer.write("file write success.text 5" + "\n");
        System.out.println("Done");
        file_writer.close();
    }

}
