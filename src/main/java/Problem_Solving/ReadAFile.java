package Problem_Solving;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ReadAFile {

    // OK

    public static void main(String[] args) throws Exception{
        ReadAFile obj = new ReadAFile();
        //obj.getCurrentDirectoryPath();

        String location = "C:\\Users\\Farhan Sadik\\Desktop\\";
        String data = readFileAsString(location + "file1.txt");
        //System.out.println("Desire Text: \n" + data);

        // reading file 1
        System.out.println();
        System.out.println("Reading integer from file 1: ");
        Scanner scanner = new Scanner(new File(location + "file1.txt"));
        int myarray[] = new int[9];
        int i = 0;

        while(scanner.hasNextInt()) {
            myarray[i++] = scanner.nextInt();
        }
        //System.out.println("my array : " + myarray[2]);

        // printing array using loop
        for (int j : myarray) {
            System.out.println(j);
        }

        // reading file 2
        System.out.println();
        System.out.println("Reading integer from file 2: ");
        Scanner scanner2 = new Scanner(new File(location + "file2.txt"));
        int mat2[] = new int[9];
        int k = 0;
        while(scanner2.hasNextInt()) {
            mat2[k++] = scanner2.nextInt();
        }
        for (int l : mat2) {
            System.out.println(l);
        }
    }

    private void calculateMatrix(int fileOne[], int fileTwo[]) {
        // null
    }

    private void getCurrentDirectoryPath() {
        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();

        System.out.println("Current Path:\n" + s);
    }

    public static String readFileAsString(String fileName)throws Exception {
        String data = "";
        data = new String(Files.readAllBytes(Paths.get(fileName)));
        return data;
    }
}
