package Problem_Solving;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Scanner;

public class BhaskarasFormula_1036 {
    public static void main(String[] args) {

        // solved
        // not submitted

        Scanner input = new Scanner(System.in);

        float A = input.nextFloat();
        float B = input.nextFloat();
        float C = input.nextFloat();
        float R1, R2;

        /*
        formula x1 = (-b + sqrt(bb) - (4ac))) / 2a
        formula x2 = (-b - sqrt(bb) - (4ac))) / 2a
        */

        if ((A == 0) || (((B*B) -(4*A*C)) < 0)) {
            System.out.print("Impossivel calcular\n");
        } else {
            R1 = (float) ((-B + Math.sqrt(((B * B) - (4 * A * C)))) / (2 * A));
            R2 = (float) ((-B - Math.sqrt(((B * B) - (4 * A * C)))) / (2 * A));

            System.out.printf("R1 = %.5f\n", R1);
            System.out.printf("R2 = %.5f\n", R2);
        }
    }
}
