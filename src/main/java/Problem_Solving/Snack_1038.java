package Problem_Solving;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Scanner;

public class Snack_1038 {
    public static void main(String[] args){

        Scanner getInput = new Scanner(System.in);

        int x = getInput.nextInt();
        int y = getInput.nextInt();

        // solved

        NumberFormat number = new DecimalFormat("#0.00");

        switch (x) {
            case 1:
                System.out.println("Total: R$ " + number.format(y * 4.00));
                break;
            case 2:
                System.out.println("Total: R$ " + number.format(y * 4.50));
                break;
            case 3:
                System.out.println("Total: R$ " + number.format(y * 5.00));
                break;
            case 4:
                System.out.println("Total: R$ " + number.format(y * 2.00));
                break;
            case 5:
                System.out.println("Total: R$ " + number.format(y * 1.50));
                break;
            default:
                System.out.println("Invalid Input");
                break;
        }
    }
}
