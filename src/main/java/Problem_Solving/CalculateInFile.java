package Problem_Solving;

import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CalculateInFile {
    public static void main(String[] args) throws Exception {

        // location
        String desktop = "C:\\Users\\Farhan Sadik\\Desktop\\";

        // getting input from file 1
        String fileName1 = getFileAsString(desktop + "file1.txt");
        System.out.println("Getting input from text file 1 : \n" + fileName1);

        // getting input from file 2
        String fileName2 = getFileAsString(desktop + "file2.txt");
        System.out.println("\nGetting input from text file 2 : \n" + fileName2);

        // converting string to integer
        int num1 = Integer.parseInt(fileName1);
        int num2 = Integer.parseInt(fileName2);

        // printing result
        System.out.print("\nSUM = " + (num1+num2));

        File outputFile = new File(desktop + "output.txt");

        // if condition - for check existence of file
        if (!outputFile.exists()) outputFile.createNewFile();

        // writing file
        FileWriter file_writer = new FileWriter(outputFile, true);
        int result = num1 + num2;
        file_writer.write("SUM = " + (num1 + num2));
        file_writer.close();
    }

    public static String getFileAsString(String fileName)throws Exception {
        String data = "";
        data = new String(Files.readAllBytes(Paths.get(fileName)));
        return data;
    }
}
