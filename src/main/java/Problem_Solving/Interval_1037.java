package Problem_Solving;

import java.util.Scanner;

public class Interval_1037 {
    public static void main(String[] args){

        // solved
        // https://draftsbook.com/uri-problem-interval-1037-solution-in-java/

        Scanner getInput = new Scanner(System.in);
        double x = getInput.nextDouble();

        //float x = getx.nextFloat();
        if (x >= 0 && x <= 25 ) {
            System.out.println("Intervalo " + "[0,25]");
        } else if (x > 25 && x <= 50) {
            System.out.println("Intervalo " + "(25,50]");
        } else if (x > 50 && x <= 75) {
            System.out.println("Intervalo " + "(50,75]");
        } else if (x > 75 && x <= 700) {
            System.out.println("Intervalo " + "(75,100]");
        } else {
            System.out.println("Fora de intervalo");
        }
    }
}
