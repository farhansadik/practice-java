package Algorithm_And_Data_Structue;

public class InsertionSort02 {
    public static int[] insertionSort(int[] input) {
        int i, j, item;

        for (i = 0; i < input.length; i = i + 1) {
            for (j = i; j > 0; j = j - 1) {
                if (input[j] < input[j-1]) {
                    item = input[j];
                    input[ j ] = input[ j - 1 ];
                    input[ j - 1 ] = item;
                }
            }

        }
        return input;
    }
    public static void main(String[] args) {

        int[] arr1 = {10,34,2,56,7,67,88,42};
        int[] arr2 = insertionSort(arr1);

        int size_of_array = arr2.length;
        for (int i= 0; i < size_of_array; i = i + 1) {
            System.out.print(" " + arr2[i]);
        }
    }

}
