package Algorithm_And_Data_Structue;

public class ArraySize {
    public static void main(String[] args) {

        String[] my_super_array = new String[5];
        String[] my_students = {"Arif", "Sohan", "Swarna", "Sanjid", "Farhan Sadik", "Mahmudul Hassan", "Nazmul Islam"};

        my_super_array[0] = "Lapato Programmer";
        my_super_array[1] = "Marato Programmer";
        my_super_array[2] = "Botot Programmer";

        int size_of_array = my_students.length;
        System.out.println("Size of 'my_students' array is : " + size_of_array);

    }

}
