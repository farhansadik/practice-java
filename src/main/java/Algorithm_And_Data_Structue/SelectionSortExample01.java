package Algorithm_And_Data_Structue;

public class SelectionSortExample01 {

    // selection_sort
    // passed
    // by tamim subeen

    public static int[] SSortME(int[] A, int n) {
        int i, j, index_min, temp;

        for (i = 0; i < n - 1; i++) {
            index_min = i;
            for (j = i + 1; j < n; j++) {
                if (A[j] < A[index_min]) {
                    index_min = j;
                }
            }
            if (index_min != i) {
                temp = A[i];
                A[i] = A[index_min];
                A[index_min] = temp;
            }
        }

        return A;
    }
    public static void main(String[] args) {
        int[] num = {10, 34, 2, 56, 7, 67, 88, 42};
        int n = num.length;
        int[] arr = SSortME(num, n);

        System.out.print("Output : ");
        for (int i: arr) {
            System.out.print(i + " ");
        }

    }

}
