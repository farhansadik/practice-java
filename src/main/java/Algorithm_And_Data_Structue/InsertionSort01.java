package Algorithm_And_Data_Structue;

public class InsertionSort01 {
    public static int[] function(int[] append) {
        int n = append.length;
        int A[] = append;

        for (int i = 1; i < n; i++) {
            int item = A[i];
            int j = i - 1;

            while (j >= 0 && A[j] > item) {
                A[j+1] = A[j];
                j = j - 1;
            }
            A[j+1] = item;
        }
        return append;
    }
    public static void main(String[] args) {
        int[] num = {5, 4, 3, 2, 1};
        int[] arr = function(num);

        for (int i: arr) {
            System.out.print(i + " ");
        }
    }

}
