package Algorithm_And_Data_Structue;

public class LinearSearchExample01 {

    // linear search
    // successful test
    // tamim subeen

    public static int LinearSearch(int[] index, int length, int target) {

        for (int i = 0; i < length; i++) {
            if (index[i] == target) {
                return i;
            }
        }

        return -1;
    }

    public static void main(String[] args) {

        int[] number = {10, 4, 6, 5, 2, 1};
        int search_index = 5;
        int array = LinearSearch(number, number.length, search_index);

        for (int i: number) {
            System.out.print(i + " ");
        }

        System.out.println("\nsearch index : " + search_index);
        System.out.println("search result : " + array);
    }

}
