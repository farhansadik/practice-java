package Practice;

import java.util.Scanner;

public class ifElse {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.print("Enter value A : ");
        int a = input.nextInt();
        System.out.print("Enter value B : ");
        int b = input.nextInt();

        if ( a <= b) {
            System.out.println(b + " is greater then " + a);
        } else if ( a >= b) {
            System.out.println(a + " is greater then " + b);
        } else {
            System.out.println("invalid items");
        }


    }
}
