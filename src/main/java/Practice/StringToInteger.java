package Practice;

import java.util.Scanner;

public class StringToInteger {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        // reading input from user
        System.out.print("A = ");
        String a = input.nextLine(); // this is a string
        System.out.print("B = ");
        String b = input.nextLine(); // this is a string

        // converting string to integer using 'Integer.parseInt()' function
        int num1 = Integer.parseInt(a);
        int num2 = Integer.parseInt(b);

        // printing output
        System.out.print("SUM = " + (num1+num2));
    }
}
