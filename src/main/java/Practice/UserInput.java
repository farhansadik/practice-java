package Practice;

import java.util.Scanner;

public class UserInput {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.print("int % ");
        int i = input.nextInt();
        input.nextLine();
        System.out.println("output : " + i);


        // not work
        System.out.print("string % ");
        String str = input.nextLine();
        System.out.println("output : " + str);

        /*
        // this one worked
        System.out.print("string again % ");
        String a = input.next();
        System.out.println("output : " + a);
        */

    }
}
