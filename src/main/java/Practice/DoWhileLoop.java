package Practice;

public class DoWhileLoop {
    public static void main(String[] args) {

        int j = 1;
        do {
            System.out.println("value of j = " + j);
            j++;
        } while (j <= 10);

    }
}
